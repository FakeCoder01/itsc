from django.urls import path
from rest_framework.authtoken.views import ObtainAuthToken
from . import views, orders

urlpatterns : list = [

    path('auth/login/', ObtainAuthToken.as_view(), name="login"),
    
    path('check/new/', views.handle_new_report, name="new_report"),
    path('check/<str:report_id>/', views.get_report_details, name="get_report_details"),
    
    
    path('check/<str:report_id>/edit/', views.handle_edit_report, name="handle_edit_report"),
    
    path('tasks/', orders.get_tasks, name="get_user_tasks"),
    path('contractors/', orders.search_contractors, name="search_contractors"),
    path('damage/<str:damage_id>/repair/', orders.handle_repair, name="handle_repair"),

    path('check/<str:report_id>/damage/<str:damage_id>/', views.get_damage_details, name="get_damage_details"),

    path('repairs/', orders.get_all_repairs, name="get_all_repairs"),
    path('applications/', views.search_report_by_road_name, name="search_by_name"),
    path('applications/<str:report_id>/damages/', views.search_report_damage_by_type, name="search_by_type"),

]
