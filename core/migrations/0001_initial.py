# Generated by Django 5.0.2 on 2024-03-28 05:42

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Contractor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('types', models.CharField(max_length=500)),
                ('phone', models.PositiveBigIntegerField()),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Damage',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category', models.CharField(max_length=20)),
                ('type', models.CharField(max_length=20)),
                ('size', models.FloatField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lat', models.FloatField()),
                ('lng', models.FloatField()),
                ('address', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='DamagedPhoto',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='damaged/')),
                ('damage', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='damaged_photos', to='core.damage')),
            ],
        ),
        migrations.CreateModel(
            name='Inspector',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=25)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='userprofile', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Repair',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(max_length=20)),
                ('description', models.CharField(blank=True, max_length=50, null=True)),
                ('date', models.DateField()),
                ('contractor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='repairs', to='core.contractor')),
                ('inspector', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='inspector_repairs', to='core.inspector')),
            ],
        ),
        migrations.CreateModel(
            name='RepairPhoto',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='repaired/')),
                ('repair', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='repaired_photos', to='core.repair')),
            ],
        ),
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('road_type', models.CharField(max_length=20)),
                ('road_length', models.PositiveSmallIntegerField()),
                ('road_coating', models.CharField(max_length=50)),
                ('damages', models.ManyToManyField(blank=True, null=True, related_name='damages', to='core.damage')),
                ('inspector', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='inspector_reports', to='core.inspector')),
                ('location', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='location_report', to='core.location')),
                ('task_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='assigned_location', to='core.location')),
            ],
        ),
        migrations.AddField(
            model_name='repair',
            name='report',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='repiar_report_q', to='core.report'),
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('road_name', models.CharField(max_length=50)),
                ('defects_count', models.PositiveSmallIntegerField(blank=True, null=True)),
                ('location', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='task_loc', to='core.location')),
            ],
        ),
    ]
