from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication, TokenAuthentication

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework import status
from .serializers import (
    TaskSerializer, ContractorSerializer, RepairOrderSerializer,
    AllRepairDetailsSerializer
)

from django.core.mail import EmailMessage
from .models import Task, Contractor, Repair, Report, Damage

# Create your views here.


@api_view(['GET'])
@permission_classes([IsAuthenticated])
@permission_classes([SessionAuthentication, TokenAuthentication])
def get_tasks(request):
    try:
        tasks = Task.objects.all()
        serializer = TaskSerializer(tasks, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    except Exception as err:
        print(err)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)



@api_view(['GET'])
@permission_classes([IsAuthenticated])
@permission_classes([SessionAuthentication, TokenAuthentication])
def search_contractors(request):
    try:
        if request.GET.get('name', '') != '':
            name = request.GET.get('name')
            contractors = Contractor.objects.filter(name__icontains=name)
            serializer = ContractorSerializer(contractors, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            contractors = Contractor.objects.all()
            serializer = ContractorSerializer(contractors, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
    
    except Exception as err:
        print(err)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    


@api_view(['GET', 'POST'])
@permission_classes([IsAuthenticated])
@permission_classes([SessionAuthentication, TokenAuthentication])
def handle_repair(request, damage_id):
    damage_id = int(damage_id)
    if Damage.objects.filter(id=damage_id).exists():
        damage = Damage.objects.get(id=damage_id)

        if request.method == 'POST':
            try:
                serializer = RepairOrderSerializer(data=request.data)
                if serializer.is_valid():

                    report = Report.objects.filter(damages__id=damage_id)[0]
                    road_name = report.task_report_application.road_name
                    serializer.save(status="need fix", road_name=road_name, inspector=request.user.userprofile, damage=damage)
                    


                    try:
                        email = EmailMessage(
                            subject = "New Reapir Work",
                            body = request.POST.get('description', 'description'),
                            from_email="ITSC <app.priyo@gmail.com>",
                            to = Contractor.objects.get(id=request.POST.get('contractor')).email
                        )
                        email.content_subtype = "html"
                        email.send()
                    except:
                        pass
                    
                    return Response(status=status.HTTP_200_OK)
                return Response(status=status.HTTP_400_BAD_REQUEST)
            except Exception as err:
                print(err)
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            try:
                serializer = RepairOrderSerializer(report)
                return Response(serializer.data, status=status.HTTP_200_OK)
            except Exception as err:
                print(err)
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)
    


@api_view(['GET'])
@permission_classes([IsAuthenticated])
@permission_classes([SessionAuthentication, TokenAuthentication])
def get_all_repairs(request):
    repairs = Repair.objects.all()
    serializer = AllRepairDetailsSerializer(repairs, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)
