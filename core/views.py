from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication, TokenAuthentication

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework import status
from .serializers import (
    ReportSerializer, ReportDetailSerialier, SearchDamageSerializer,
    SearchRoadNameSerializer, DamageSerializer, PhotoSerializer
)
from .models import Report, Damage, Location, Task, Repair, DamagedPhoto

# Create your views here.


@api_view(['POST'])
@permission_classes([IsAuthenticated])
@permission_classes([SessionAuthentication, TokenAuthentication])
def handle_new_report(request):
    try:
        serializer = ReportSerializer(data=request.data)
        if serializer.is_valid():

            try:
                task_id = request.data.get('idApplication'),
                t = Task.objects.get(id=task_id)
        
                road = request.data.get('road')
                report = Report.objects.create(
                    inspector = request.user.userprofile,
                    road_type = road.get('type'),
                    road_length = road.get('length'),
                    road_coating = road.get('coating')
                )
                t.report_id = report.id
                
            except Exception as err:
                print(err)
                return Response({'detail' : 'error while creating report'}, status=status.HTTP_400_BAD_REQUEST)

            try:
                damages = request.data.get('damages')
                for damage in damages:
                    _location = Location.objects.create(
                        lat = damage.get('coordinates').get('lat'),
                        lng = damage.get('coordinates').get('lng'),
                        address = damage.get('address')
                    )
                    _damage = Damage.objects.create(
                        location = _location,
                        category = damage.get('category'),
                        type = damage.get('type'),
                        size = damage.get('size', None)
                    )

                    # for photo in damage.photos:
                    #     p = DamagedPhoto.objects.create(
                    #         image = photo.FILES['image'],
                    #         damage = _damage
                    #     )

                    report.damages.add(_damage)
                t.defects_count = len(damages)
                t.save()
            except Exception as err:
                print(err)
                return Response({'detail' : 'damages are invalid'}, status=status.HTTP_400_BAD_REQUEST)
            
            r = ReportSerializer(report)
            return Response(r.data, status=status.HTTP_200_OK)
        
        return Response(status=status.HTTP_400_BAD_REQUEST)
    

    except Exception as err:
        print(err)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
@permission_classes([SessionAuthentication, TokenAuthentication])
def handle_edit_report(request, report_id):

    if not Report.objects.filter(id=report_id).exists():
        return Response(status=status.HTTP_404_NOT_FOUND)
    

    report = Report.objects.get(id=report_id)

    try:
        serializer = ReportSerializer(data=request.data)
        if serializer.is_valid():

            return Response(status=status.HTTP_200_OK)       
        return Response(status=status.HTTP_400_BAD_REQUEST)
    

    except Exception as err:
        print(err)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
def get_report_details(request, report_id):
    try:
        if not Report.objects.filter(id=report_id).exists():
            return Response(status=status.HTTP_404_NOT_FOUND)
        report = Report.objects.get(id=report_id)

        serializer = ReportDetailSerialier(report)
        return Response(serializer.data, status=status.HTTP_200_OK)
    except:
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)



@api_view(['GET'])
@permission_classes([IsAuthenticated])
@permission_classes([SessionAuthentication, TokenAuthentication])
def get_damage_details(resuest, report_id, damage_id):
    if not Report.objects.filter(id=report_id).exists() or not Damage.objects.filter(id=damage_id).exists():
        return Response(status=status.HTTP_404_NOT_FOUND)
    damage = Damage.objects.get(id=damage_id)

    serializer = DamageSerializer(damage)
    return Response(serializer.data, status=status.HTTP_200_OK)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
@permission_classes([SessionAuthentication, TokenAuthentication])
def search_report_damage_by_type(request, report_id):

    report_id = int(report_id)
    if not Report.objects.filter(id=report_id).exists():
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    report = Report.objects.get(id=report_id)
    
    if request.GET.get('damage_type', '') == '':
        damages = report.damages.all()
    else:
        category = request.GET.get('damage_type')
        damages = report.damages.filter(category__icontains=category)
    serializer = SearchDamageSerializer(damages, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)



@api_view(['GET'])
@permission_classes([IsAuthenticated])
@permission_classes([SessionAuthentication, TokenAuthentication])
def search_report_by_road_name(request):
    if request.GET.get('road_name', '') == '':
        tasks = Task.objects.all()
        serializer = SearchRoadNameSerializer(tasks, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    else:
        road_name = request.GET.get('road_name')
        tasks = Task.objects.filter(road_name__icontains=road_name)
        serializer = SearchRoadNameSerializer(tasks, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


def  get_damage_photos(reuest, damage_id):
    damage_id = int(damage_id)
    if not Damage.objects.filter(id=damage_id).exists():
        return Response(status=status.HTTP_404_NOT_FOUND)
    damage = Damage.objects.get(id=damage_id)
    photos = DamagedPhoto.objects.filter(damage=damage)
    serializer = PhotoSerializer(photos, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)



def  get_report_photos(reuest, report_id):
    report_id = int(report_id)
    if not Damage.objects.filter(id=report_id).exists():
        return Response(status=status.HTTP_404_NOT_FOUND)
    damage = Damage.objects.get(id=report_id)
    photos = DamagedPhoto.objects.filter(damage=damage)
    serializer = PhotoSerializer(photos, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)
