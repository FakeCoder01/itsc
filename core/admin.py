from django.contrib import admin
from .models import (
    Inspector, Contractor, Location, Damage, DamagedPhoto, 
    Repair, RepairPhoto, Report, Task
)

# Register your models here.

admin.site.site_header = "ITSC Web"
admin.site.site_title = "Administrator ITSC Web"

admin.site.register(Inspector)
admin.site.register(Contractor)
admin.site.register(Location)
admin.site.register(Damage)
admin.site.register(DamagedPhoto)
admin.site.register(Repair)
admin.site.register(RepairPhoto)
admin.site.register(Report)
admin.site.register(Task)


admin.site.name = "ITSC Web"