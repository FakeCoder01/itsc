from django.db import models
from django.contrib.auth.models import User

# Create your models here.




class Inspector(models.Model):
    user = models.OneToOneField(User, related_name="userprofile", on_delete=models.CASCADE)
    name = models.CharField(max_length=25)

    def __str__(self) -> str:
        return self.name


class Contractor(models.Model):
    name = models.CharField(max_length=30)
    types = models.CharField(max_length=500)
    phone = models.PositiveBigIntegerField()
    email = models.EmailField(null=True, blank=True)

    
    def __str__(self) -> str:
        return self.name 


class Location(models.Model):
    lat = models.FloatField()
    lng = models.FloatField()
    address = models.CharField(max_length=200)

    def __str__(self) -> str:
        return self.address
    

class Damage(models.Model):
    location = models.OneToOneField(Location, related_name="location_report", on_delete=models.CASCADE, null=True, blank=True)
    category = models.CharField(max_length=20)
    type = models.CharField(max_length=20)
    size = models.FloatField(null=True, blank=True)

    def __str__(self) -> str:
        return str(self.id)

class DamagedPhoto(models.Model):
    image = models.ImageField(upload_to="damaged/")
    damage = models.ForeignKey(Damage, related_name="damaged_photos", on_delete=models.CASCADE)

    def __str__(self) -> str:
        return str(self.id)




# https://nominatim.openstreetmap.org/reverse?format=json&lat=58.006635&lon=56.288215&zoom=18&addressdetails=1


class Report(models.Model):
    damages = models.ManyToManyField(Damage, related_name="reports", null=True, blank=True)
    inspector = models.ForeignKey(Inspector, related_name="inspector_reports", on_delete=models.CASCADE)

    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)


    road_type = models.CharField(max_length=20)
    road_length = models.PositiveSmallIntegerField()
    road_coating = models.CharField(max_length=50)
 
   

    def __str__(self) -> str:
        return str(self.id)



class Task(models.Model):
    location = models.ForeignKey(Location, related_name="task_loc", on_delete=models.CASCADE)
    road_name = models.CharField(max_length=50)
    defects_count = models.PositiveSmallIntegerField(blank=True, null=True)
    report_id = models.OneToOneField(Report, related_name="task_report_application", null=True, blank=True, on_delete=models.CASCADE)



class Repair(models.Model):
    status = models.CharField(max_length=20, default="need fix")
    road_name = models.CharField(max_length=200, null=True, blank=True)
    description = models.CharField(max_length=50, blank=True, null=True)
    inspector = models.ForeignKey(Inspector, related_name="inspector_repairs", on_delete=models.CASCADE, blank=True)
    contractor = models.ForeignKey(Contractor, related_name="repairs",on_delete=models.CASCADE)
    date = models.DateField()
    damage = models.ForeignKey(Damage, related_name="damage_repair_q", on_delete=models.CASCADE, null=True, blank=True)
    # defect

    def __str__(self) -> str:
        return str(self.id)




class RepairPhoto(models.Model):
    image = models.ImageField(upload_to="repaired/")
    repair = models.ForeignKey(Repair, related_name="repaired_photos", on_delete=models.CASCADE)
    
    
    def __str__(self) -> str:
        return str(self.id)




