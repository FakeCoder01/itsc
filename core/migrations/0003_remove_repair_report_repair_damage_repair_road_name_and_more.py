# Generated by Django 5.0.2 on 2024-03-29 04:43

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_remove_report_location_remove_report_task_id_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='repair',
            name='report',
        ),
        migrations.AddField(
            model_name='repair',
            name='damage',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='damage_repair_q', to='core.damage'),
        ),
        migrations.AddField(
            model_name='repair',
            name='road_name',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='report',
            name='damages',
            field=models.ManyToManyField(blank=True, null=True, related_name='reports', to='core.damage'),
        ),
        migrations.AlterField(
            model_name='task',
            name='report_id',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='task_report_application', to='core.report'),
        ),
    ]
