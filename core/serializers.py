from rest_framework import serializers
from .models import (
    Damage, Report, Repair, Location, Contractor, Task,
    DamagedPhoto
)



class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Location



class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("image",)
        model = DamagedPhoto



class DamageSerializer(serializers.ModelSerializer):
    location = LocationSerializer()
    damaged_photos = PhotoSerializer(many=True, read_only=True)

    class Meta:
        fields = "__all__"
        model = Damage

class ReportSerializer(serializers.ModelSerializer):
    damages = DamageSerializer(many=True)
    class Meta:
        fields = "__all__"


class TaskSerializer(serializers.ModelSerializer):
    coordinates = LocationSerializer(source="location", read_only=True)
    address = serializers.CharField(source="location.address", read_only=True)

    class Meta:
        fields = ["id", "address", "coordinates", "road_name", "defects_count", "report_id"]
        model = Task


class ContractorSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Contractor


class RepairOrderSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Repair


class ReportDetailSerialier(serializers.ModelSerializer):
    damages = DamageSerializer(many=True)
    class Meta:
        fields = "__all__"
        model = Report


class SearchDamageSerializer(serializers.ModelSerializer):
    damage_type = serializers.CharField(source="category")
    class Meta:
        fields = ["id", "damage_type"]
        model = Damage



class SearchRoadNameSerializer(serializers.ModelSerializer):
    report_id = serializers.CharField(source="report_id.id", read_only=True)
    class Meta:
        fields = ["road_name", "report_id"]
        model = Task


class AllRepairDetailsSerializer(serializers.ModelSerializer):
    defect_type = serializers.CharField(source="damage.type")
    contractor_name = serializers.CharField(source="contractor.name", read_only=True)
    deadline = serializers.DateField(source="date")

    class Meta:
        fields = ["id", "defect_type", "road_name", "contractor_name", "deadline"]
        model = Repair







